
# RevOS AOSP Q #

### Sync ###

```bash

# Initialize local repository
repo init -u ssh://git@gitlab.com/revos-aospq/base/manifest -b ten

# Sync
repo sync -c -j$(nproc --all) --force-sync --no-clone-bundle --no-tags
```

### Build ###

```bash

# Set up environment
$ . build/envsetup.sh

# Choose a target
$ brunch aosp_$device-userdebug

```
